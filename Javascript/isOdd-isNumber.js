
/*
 * Originally written by Jon Schlinkert. I just combined them. No need to use a library and dependencies for a task like this.
 */

/*!
 * is-odd <https://github.com/jonschlinkert/is-odd>
 * is-number <https://github.com/jonschlinkert/is-number>
 * 
 * Copyright (c) 2014-present, Jon Schlinkert.
 * Released under the MIT License.
 */

'use strict';

const isNumber = function(num) {
  if (typeof num === 'number') {
    return num - num === 0;
  }
  if (typeof num === 'string' && num.trim() !== '') {
    return Number.isFinite ? Number.isFinite(+num) : isFinite(+num);
  }
  return false;
};

const isOdd = function(value) {
  const n = Math.abs(value);
  if (!isNumber(n)) {
    throw new TypeError('expected a number');
  }
  if (!Number.isInteger(n)) {
    throw new Error('expected an integer');
  }
  if (!Number.isSafeInteger(n)) {
    throw new Error('value exceeds maximum safe integer');
  }
  return (n % 2) === 1;
};

isOdd(23) // true
isOdd(22) // false
isNumber(23) // true
isNumber(23.1234) // true
isNumber(23.1234,736) // true
isNumber("23.1234,736") // false

