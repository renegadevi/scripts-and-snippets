// Scroll to top. Something that a user expects.
router.beforeEach(function(to, from, next) {
    window.scrollTo(0, 0);
    next();
});
