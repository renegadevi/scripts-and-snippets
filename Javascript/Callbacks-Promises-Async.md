# Callbacks, Promises and Async (await)

If you come from Python, writing code using try-catch with Async await is the most familiar way to wrap your head around it.

| | Callbacks | Promises | Async/await |
|-|-----------|----------|-------------|
| Recommended|No|Yes|Yes|
| Performance|Faster|Slower|Slower|
| When to use |Avoid, but can be used when next task depends on the result of previous task|Multiple async tasks in parallell|Multiple async task in sequence|
| Syntax |Functions passed as arguments to other functions|Constructor function that returns a promise|async keyword before a function, await keyword to resolve a promise
| Readability|Can become deeply nested and difficult to read|Chaining syntax `.then()`|Synchronous-looking code |
|Parallelization|Can be difficult to parallelize without extra code|Can be parallelized with `Promise.all()`| Can be parallelized with `Promise.all()` or for-await-of loop
| Error handling |Requires separate error-handling logic for each call| `.catch()` |Try/catch blocks|

Benchmark: https://gist.github.com/weyoss/24f9ecbda175d943a48cb7ec38bde821

---
## Simple syntax/code comparision (reading a file)
### Callbacks
```js
const fs = require('fs');

function readFile(callback) {
  fs.readFile('data.txt', 'utf8', function(error, data) {
    if (error) {
      callback(error);
    } else {
      callback(null, data);
    }
  });
}

readFile(function(error, data) {
  if (error) {
    console.error(error);
  } else {
    console.log(data);
  }
});
```
### Promise
```js
const fs = require('fs').promises;

function readFile() {
  return fs.readFile('data.txt', 'utf8');
}

readFile()
  .then(data => console.log(data))
  .catch(error => console.error(error));
```
### Async await
```js
const fs = require('fs').promises;

async function readFile() {
  try {
    const data = await fs.readFile('path/to/file.txt', 'utf8');
    console.log(data);
  } catch (error) {
    console.error(error);
  }
}

readFile();
```

## Code example using Vue 3 (Async await + axios)
```js
<script setup>
import { ref, onMounted } from 'vue';
import axios from 'axios';

const users = ref([]);

async function fetchUsers() {
  try {
    const response = await axios.get('https://api.example.com/users');
    users.value = response.data;
  } catch (error) {
    console.error(error);
  }
}

onMounted(fetchUsers);
</script>
<template>
  <div>
    <ul>
      <li v-for="user in users" :key="user.id">{{ user.name }}</li>
    </ul>
  </div>
</template>
```

## Other links

### Comparing Callbacks, Promises and Async Await in TypeScript
https://www.johnpapa.net/async-comparisons/


### Async Components
https://vuejs.org/guide/components/async.html

#### Async in Nuxt
https://nuxt.com/docs/api/composables/use-async-data


### How to use promises
https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Asynchronous/Promises#async_and_await


### Should I Use Promises or Async await
https://betterprogramming.pub/should-i-use-promises-or-async-await-126ab5c98789

