# Useful NPM libraries

## mkcert
Use mkcert to provide certificate support for vite https development services.

https://www.npmjs.com/package/vite-plugin-mkcert


## js-console-formatter
Simple formatter that adds timestamps, symbols and printed in a more structural formatting

https://www.npmjs.com/package/js-console-formatter


## localforage
localForage improves the offline experience of your web app by using asynchronous storage (IndexedDB or WebSQL) with a simple, localStorage-like API.


Use IndexedDB like localstorage to expand your local storage limit.

https://www.npmjs.com/package/localforage


## ~~lodash~~ Radash 
Functional utility library - modern, simple, typed, powerful

https://www.npmjs.com/package/radash


## VueUse
Collection of essential Vue Composition Utilities

https://www.npmjs.com/package/@vueuse/core

## Axios
Promise based HTTP client for the browser and node.js

https://www.npmjs.com/package/axios

## Others...

- eslint: https://www.npmjs.com/package/eslint
- prettier: https://www.npmjs.com/package/prettier
- tslib: https://www.npmjs.com/package/tslib