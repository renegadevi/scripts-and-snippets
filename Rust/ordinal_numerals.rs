/// Converts a number into its ordinal suffix (e.g., 1 becomes "st", 2 becomes "nd").
///
/// # Arguments
///
/// * `x` - A 32-bit unsigned integer representing the number to be converted.
///
/// # Returns
///
/// * A `String` containing the ordinal suffix (`st`, `nd`, `rd`, or `th`).
///
/// # Examples
///
/// ```
/// assert_eq!(ordinal(1), "st");
/// assert_eq!(ordinal(11), "th");
/// assert_eq!(ordinal(111), "th");
/// assert_eq!(ordinal(121), "st");
/// assert_eq!(ordinal(20), "th");
/// assert_eq!(ordinal(52), "nd");
/// ```
fn ordinal(x: u32) -> String {
    match (x % 10, x % 100) {
        (_, 11..=13) => "th".to_string(),  // Special case for numbers ending in 11, 12, or 13
        (1, _) => "st".to_string(),        // Numbers ending in 1
        (2, _) => "nd".to_string(),        // Numbers ending in 2
        (3, _) => "rd".to_string(),        // Numbers ending in 3
        _ => "th".to_string(),             // All other numbers
    }
}
