use std::collections::BinaryHeap;
use std::cmp::Reverse;
use std::time::Instant; // Used for time benchmarking

/// This function returns the largest possible sum of elements in a list after performing k negations.
/// It negates the smallest elements from the list, as many times as allowed by k.
/// This code I converted from a python script I had written called `largest_sum`
///
/// # Arguments
/// * `nums` - A vector of integers.
/// * `k` - An integer representing the number of negations.
///
/// # Returns
/// An integer which is the largest sum of elements after `k` negations.
///
/// # Examples
/// ```
/// assert_eq!(largest_sum_after_k_negations(vec![4, 2, 3], 1), 5);
/// assert_eq!(largest_sum_after_k_negations(vec![3, -1, 0, 2], 3), 6);
/// assert_eq!(largest_sum_after_k_negations(vec![2, -3, -1, 5, -4], 2), 13);
/// ```
fn largest_sum_after_k_negations(nums: Vec<i32>, k: usize) -> i32 {
    // Convert the vector into a min-heap using Reverse to simulate min-heap behavior
    let mut heap: BinaryHeap<Reverse<i32>> = nums.into_iter().map(Reverse).collect();

    // Perform k negations on the smallest element
    for _ in 0..k {
        if let Some(Reverse(smallest)) = heap.pop() {
            heap.push(Reverse(-smallest)); // Negate the smallest element and push back
        }
    }

    // Calculate the sum of the heap
    heap.into_iter().map(|Reverse(x)| x).sum()
}


fn largest_sum_after_k_negations2(mut nums: Vec<i32>, mut k: usize) -> i32 {

    // Flip negative numbers as long as `k` allows
    nums.sort();
    for i in 0..nums.len() {
        if nums[i] < 0 && k > 0 {
            nums[i] = -nums[i];
            k -= 1;
        }
    }

    // If `k` is still odd, flip the smallest number (after sorting, it will be at index 0)
    if k % 2 == 1 {
        // Flip the smallest element (min will be at index 0 after sort)
        let min_index = nums.iter().enumerate().min_by_key(|&(_, v)| v).unwrap().0;
        nums[min_index] = -nums[min_index];
    }

    // Return the sum of the array
    nums.iter().sum()
}

fn main() {

    // Running the examples
    let start = Instant::now();
    println!("Example 1, output = {}", largest_sum_after_k_negations(vec![4, 2, 3], 1));
    println!("Example 2, output = {}", largest_sum_after_k_negations(vec![3, -1, 0, 2], 3));
    println!("Example 3, output = {}", largest_sum_after_k_negations(vec![2, -3, -1, 5, -4], 2));
    println!("Time taken by main: {:?} \n", start.elapsed());

    let start2 = Instant::now();
    println!("Example 1, output = {}", largest_sum_after_k_negations2(vec![4, 2, 3], 1));
    println!("Example 2, output = {}", largest_sum_after_k_negations2(vec![3, -1, 0, 2], 3));
    println!("Example 3, output = {}", largest_sum_after_k_negations2(vec![2, -3, -1, 5, -4], 2));
    println!("Time taken by main: {:?} \n", start2.elapsed());
 }


// On a Macbook Pro M1 Pro - 
// Rust 1.79.0
//  $ ./largest_sum
//  Example 1, output = 5
//  Example 2, output = 6
//  Example 3, output = 13
//  Time taken by main: 283.834µs
//
//  Example 1, output = 5
//  Example 2, output = 6
//  Example 3, output = 13
//  Time taken by main: 10.959µs
//
// Python version (3.12.4)
//  $ python3 largest_sum.py
//  Expected output = 5, Actual output = 5
//  Expected output = 6, Actual output = 6
//  Expected output = 13, Actual output = 13
//  Total time taken by script: 39.792 µs




