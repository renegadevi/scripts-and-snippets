use std::fs::File;
use std::io::{self, Read, Write};

/// Reads the contents of a file into a string.
///
/// # Arguments
///
/// * `path` - A string slice that holds the file path.
///
/// # Returns
///
/// * `Ok(String)` containing the file's content if successful.
/// * `Err(io::Error)` if the file could not be opened or read.
///
/// # Example
///
/// ```
/// let contents = read_file_to_string("path/to/file.txt").unwrap();
/// println!("{}", contents);
/// ```
fn read_file_to_string(path: &str) -> io::Result<String> {
    let mut file = File::open(path)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    Ok(contents)
}

/// Writes a string to a file.
///
/// # Arguments
///
/// * `path` - A string slice representing the file path.
/// * `contents` - The contents to write to the file.
///
/// # Returns
///
/// * `Ok(())` if the file was successfully written.
/// * `Err(io::Error)` if the file could not be written.
///
/// # Example
///
/// ```
/// write_string_to_file("path/to/file.txt", "Hello, world!").unwrap();
/// ```
fn write_string_to_file(path: &str, contents: &str) -> io::Result<()> {
    let mut file = File::create(path)?;
    file.write_all(contents.as_bytes())?;
    Ok(())
}
