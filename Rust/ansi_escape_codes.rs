
/// Colorize text using ANSI escape codes
///
/// # Arguments
///
/// * `text` - The text to be colorized.
/// * `color_code` - The ANSI color code as a string.
///
/// # Returns
///
/// * A `String` containing the colorized text.
///
/// # ANSI Color Codes:
///
/// - **Foreground Colors**:
///   - `30` - Black
///   - `31` - Red
///   - `32` - Green
///   - `33` - Yellow
///   - `34` - Blue
///   - `35` - Magenta
///   - `36` - Cyan
///   - `37` - White
///
/// - **Background Colors** (add `10` to the foreground code):
///   - `40` - Black
///   - `41` - Red
///   - `42` - Green
///   - `43` - Yellow
///   - `44` - Blue
///   - `45` - Magenta
///   - `46` - Cyan
///   - `47` - White
///
/// - **Text Styles**:
///   - `0`  - Reset / Normal
///   - `1`  - Bold
///   - `3`  - Italic (not widely supported)
///   - `4`  - Underline
///   - `7`  - Inverse (swap foreground and background)
///   - `9`  - Strikethrough (not widely supported)
///
/// # Examples
///
/// ```
/// println!("{}", colorize("Hello, world!", "31")); // Red text
/// ```
/// ```
/// for line in &lines {
///     println!("{}", colorize(line, color_code));
/// }
/// ```
fn colorize(text: &str, color_code: &str) -> String {
    format!("\x1B[{}m{}\x1B[0m", color_code, text)
}


/// Clears the terminal screen using ANSI escape codes.
///
/// This function clears the entire terminal screen and moves the cursor to the top-left corner.
///
/// # ANSI Escape Codes Used:
///
/// - `\x1B[2J` - Clears the entire screen.
/// - `\x1B[H`  - Moves the cursor to the top-left corner (position 0,0).
///
/// # Example
///
/// ```
/// clear_screen();  // Clears the terminal screen
/// ```
fn clear_screen() {
    print!("\x1B[2J\x1B[H");
}


/// Clears the current line in the terminal.
///
/// # ANSI Escape Code:
///
/// - `\x1B[2K` - Clears the entire current line.
///
/// # Example
///
/// ```
/// clear_line();  // Clears the current line
/// println!("Line cleared!");
/// ```
fn clear_line() {
    print!("\x1B[2K");
}


/// Sets the text style using ANSI escape codes.
///
/// # Arguments
///
/// * `style_code` - The ANSI style code for the desired text style.
///
/// # ANSI Style Codes:
///   - `0`  - Reset / Normal
///   - `1`  - Bold
///   - `3`  - Italic (not widely supported)
///   - `4`  - Underline
///   - `7`  - Inverse (swap foreground and background)
///
/// # Example
///
/// ```
/// set_text_style("1");  // Sets the text to bold
/// println!("This is bold text!");
/// ```
fn set_text_style(style_code: &str) {
    print!("\x1B[{}m", style_code);
}