use std::env;

/// Fetches the value of an environment variable.
///
/// # Arguments
///
/// * `key` - The name of the environment variable.
///
/// # Returns
///
/// * `Some(String)` if the environment variable exists.
/// * `None` if the environment variable is not found.
///
/// # Example
///
/// ```
/// if let Some(val) = get_env_variable("HOME") {
///     println!("Home directory: {}", val);
/// }
/// ```
fn get_env_variable(key: &str) -> Option<String> {
    env::var(key).ok()
}
