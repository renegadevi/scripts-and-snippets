use serde::Deserialize;
use serde_json;

/// Parses a JSON string into a struct.
///
/// # Arguments
///
/// * `json_data` - A string slice containing JSON data.
///
/// # Returns
///
/// * `Ok(JsonData)` with the parsed struct if successful.
/// * `Err(serde_json::Error)` if the parsing fails.
///
/// # Example
///
/// ```
/// let json = r#"{"key": "example", "value": 42}"#;
/// let data = parse_json(json).unwrap();
/// println!("Key: {}, Value: {}", data.key, data.value);
/// ```
#[derive(Deserialize)]
struct JsonData {
    key: String,
    value: i32,
}

fn parse_json(json_data: &str) -> Result<JsonData, serde_json::Error> {
    let parsed: JsonData = serde_json::from_str(json_data)?;
    Ok(parsed)
}
